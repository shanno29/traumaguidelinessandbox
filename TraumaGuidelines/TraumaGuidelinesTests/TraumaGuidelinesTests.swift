//
//  TraumaGuidelinesTests.swift
//  TraumaGuidelinesTests
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest
@testable import TraumaGuidelines

class RegisterPresenterTest: XCTestCase {
    
    class RegisterTestView: RegisterView {
        
        func onResponseWithDialog(doOnSuccess: @escaping (String, String) -> (), doOnError: @escaping (String, String) -> ()) -> NetworkResult {
            return { _, _, _ in }
        }
        
        func onResponse(doOnSuccess: @escaping () -> (), doOnError: @escaping (String, String) -> ()) -> NetworkResult {
            return { _, _, _ in }
            
        }
        
        func showInfo(_ title: String, _ msg: String, _ action: Dialog.Callback?) {}
        
        func showError(_ title: String, _ msg: String, action: Dialog.Callback?) {}
        
        func showError(_ title: String, _ msg: String) {}
        
        func goBack() {}
        
    }
    
    var presenter: RegisterPresenter?
    var view: RegisterView!
    
    override func setUp() {
        super.setUp()
        view = RegisterTestView()
        presenter = RegisterPresenter(view: view)
        
    }
    
    override func tearDown() {
        presenter = nil;
        super.tearDown()
    }
    
    func testNotNull() {
        XCTAssert(presenter != nil)
    }
    
}
