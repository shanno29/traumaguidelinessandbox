//
//  HomeTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/9/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class HomeTest: BaseTest {
  
  lazy var topicsHomeNavBar = app.navigationBars["Topics"]
  lazy var topicNavBar = app.navigationBars["Example Topic"]
  lazy var firstCell = app.staticTexts["Example Topic"]
  
  override func setUp() {
    super.setUp()
    loginUser()
  }
  
  func tests() {
    trySelectTopic()
    tryBackButton()
  }
  
  private func trySelectTopic() {
    /* Given */
    XCTAssert(topicsHomeNavBar.exists)
    
     /* Then */
    firstCell.tap()
    XCTAssert(topicNavBar.exists)
  }
  
  private func tryBackButton() {
    /* Given */
    XCTAssert(topicNavBar.exists)

    /* Then */
    topicNavBar.tapToolbarItem("back")
    XCTAssert(topicsHomeNavBar.exists)
  }
  
}
