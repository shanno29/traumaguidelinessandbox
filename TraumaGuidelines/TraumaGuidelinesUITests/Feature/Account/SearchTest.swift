//
//  FeedbackTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/15/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class SearchTest: BaseTest {
    
    override func setUp() {
        super.setUp()
        loginUser()
    }
    
    func tests() {
        
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like."]/*[[".cells.staticTexts[\"This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like.\"]",".staticTexts[\"This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like.\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let exampleTopicNavigationBar = app.navigationBars["Example Topic"]
        let searchButton = exampleTopicNavigationBar.buttons["search"]
        searchButton.tap()
        
        let searchAlert = app.alerts["Search"]
        searchAlert.collectionViews.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .textField).element.typeText("thoracic")
        searchAlert.buttons["Search"].tap()
        app.navigationBars["'thoracic'"].buttons["Clear"].tap()
        exampleTopicNavigationBar.buttons["settings"].tap()
        
        let searchAllTopicsSwitch = tablesQuery/*@START_MENU_TOKEN@*/.switches["Search All Topics"]/*[[".cells[\"0\"].switches[\"Search All Topics\"]",".switches[\"Search All Topics\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        searchAllTopicsSwitch.tap()
        searchAllTopicsSwitch.tap()
        searchAllTopicsSwitch.swipeRight()
        exampleTopicNavigationBar.buttons["back"].tap()
        searchButton.tap()
        

    }
    
}
