//
//  FeedbackTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/15/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class FeedbackTest: BaseTest {

  override func setUp() {
    super.setUp()
    loginUser()
  }
  
  func tests() {
    
    // go to example topic
    app.tables/*@START_MENU_TOKEN@*/.staticTexts["This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like."]/*[[".cells.staticTexts[\"This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like.\"]",".staticTexts[\"This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like.\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
    app.navigationBars["Example Topic"].buttons["feedback"].tap()
    
    // should show blank reason dialog
    app.buttons["Submit"].tap()
    app.alerts["Blank Reason"].buttons["Close"].tap()
    
    app.tables/*@START_MENU_TOKEN@*/.staticTexts["Spelling Error"]/*[[".cells.staticTexts[\"Spelling Error\"]",".staticTexts[\"Spelling Error\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
    app.buttons["Submit"].tap()

    // should show blank description dialog
    app.alerts["Blank Description"].buttons["Close"].tap()
    app.textViews["Description"].replaceText("asdf")
    app.buttons["Submit"].tap()
    
    // check back button
    app.navigationBars["Example Topic"].buttons["feedback"].tap()
    app.buttons["Cancel"].tap()
    
  }
  
}


