//
//  FeedbackTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/15/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class ContentViewTest: BaseTest {
    
    override func setUp() {
        super.setUp()
        loginUser()
    }
    
    func tests() {
       
        // select example topic
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like."]/*[[".cells.staticTexts[\"This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like.\"]",".staticTexts[\"This is an example topic. It obvious should be deleted before production. However, it is very useful to illustrate what an example topic could look like.\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["This is a great example of a subtopic."]/*[[".cells.staticTexts[\"This is a great example of a subtopic.\"]",".staticTexts[\"This is a great example of a subtopic.\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        // go through each page
        let nextButton = app.buttons["Next"]
        nextButton.tap()
        nextButton.tap()
        nextButton.tap()
        nextButton.tap()
        
        // go back from each page
        let backButton = app.buttons["Back"]
        backButton.tap()
        backButton.tap()
        backButton.tap()
        backButton.tap()
        
        // go to feedback
        let subTopicOneNavBar = app.navigationBars["Example Subtopic 1"]
        subTopicOneNavBar.buttons["feedback"].tap()
        
        app.children(matching: .window)
            .element(boundBy: 0)
            .children(matching: .other)
            .element(boundBy: 1)
            .children(matching: .other)
            .element.children(matching: .other)
            .element.children(matching: .other)
            .element.children(matching: .other)
            .element.tap()
        
        // go back from feedback
        app.buttons["Cancel"].tap()
        subTopicOneNavBar.buttons["back"].tap()
    }
    
}



