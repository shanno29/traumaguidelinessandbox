//
//  SettingsTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/14/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import Foundation
import XCTest

class SettingsTest: BaseTest {
  
  lazy var settingsNavBar = app.navigationBars["TraumaGuidelines.SettingsView"]
  
  override func setUp() {
    super.setUp()
    loginUser()
    app.navigationBars["Topics"].buttons["settings"].tap()
    
  }
  
  func testSettings() {
    //tryHasUserInfo()
    
    //tryUpdateContent()
    //tryToggleSearchAll()
    
    tryLogoutButton()
    
  }

  private func tryHasUserInfo() {
    /* Given */
    XCTAssert(settingsNavBar.exists)
    
    /* Then */
    //XCTAssert(app.tables.staticTexts[Config.name].exists)
    //XCTAssert(app.tables.staticTexts[Config.email].exists)
  }
  
  private func tryUpdateContent() {
    
  }
  
  private func tryToggleSearchAll() {
    
  }
  
  private func tryLogoutButton() {
    /* Given */
    XCTAssert(settingsNavBar.exists)

    /* Then */
    settingsNavBar.tapToolbarItem("Sign Out")
    XCTAssert(!settingsNavBar.exists)
  }
  
}

