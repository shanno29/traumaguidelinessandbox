//
//  RegisterTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class LoginTest: BaseTest {
  
  lazy var submitButton = app.buttons["Login"]
  lazy var cancelButton = app.buttons["Cancel"]
  
  lazy var emailField = app.textFields["Email"]
  lazy var passwordField = app.secureTextFields["Password"]
  
  func tests() {
    tryBlankEmail()
    tryBlankPassword()
    tryUnknownUser()
  }
  
  private func tryBlankEmail() {
    /* Given */
    emailField.replaceText("")
    passwordField.replaceText("")
    submitButton.tap()

    /* Then */
    app.alerts["Blank Email"].waitForDialogAndTap("Close")
  }
  
  private func tryBlankPassword() {
    /* Given */
    emailField.replaceText("testEmail")
    passwordField.replaceText("")
    submitButton.tap()
      
    /* Then */
    app.alerts["Blank Password"].waitForDialogAndTap("Close")
  }

  private func tryUnknownUser() {
    /* Given */
    emailField.replaceText("testEmail")
    passwordField.replaceText("testPassword")
    submitButton.tap()
    
    /* Then */
    app.alerts["Invalid Input"].waitForDialogAndTap("Close")
  }
  
}
