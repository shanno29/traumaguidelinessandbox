//
//  RegisterTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class RegisterTest: BaseTest {
  
  lazy var submitButton = app.buttons["Register"]
  lazy var cancelButton = app.buttons["Cancel"]
  
  lazy var nameField = app.textFields["Name"]
  lazy var emailField = app.textFields["Email"]
  
  override func setUp() {
    super.setUp()
    app.buttons["Register"].tap()
  }
  
  func tests() {
    tryBlankName()
    tryBlankEmail()
    tryBadEmail()
    tryExistingUser()
    tryGoodUser()
    tryCancelButton()
  }
  
  private func tryBlankName() {
    /* Given */
    nameField.replaceText("")
    emailField.replaceText("")
    submitButton.tap()
      
    /* Then */
    app.alerts["Blank Name"].waitForDialogAndTap("Close")
  }
  
  private func tryBlankEmail() {
    /* Given */
    nameField.replaceText("testName")
    emailField.replaceText("")
    submitButton.tap()

    /* Then */
    app.alerts["Blank Email"].waitForDialogAndTap("Close")
  }
  
  private func tryBadEmail() {
    /* Given */
    nameField.replaceText("testName")
    emailField.replaceText("testEmail")
    submitButton.tap()

    /* Then */
    app.alerts["Invalid Input"].waitForDialogAndTap("Close")
  }
  
  private func tryExistingUser() {
    /* Given */
    nameField.replaceText(Config.name)
    emailField.replaceText(Config.email)
    submitButton.tap()

    /* Then */
    app.alerts["Registration Failed"].waitForDialogAndTap("Close")
  }
  
  private func tryGoodUser() {
    /* Given */
    nameField.replaceText(Config.randomString())
    emailField.replaceText(Config.randomString()+"@uwm.edu")
    submitButton.tap()

    /* Then */
    app.alerts["Registration Submitted"].waitForDialogAndTap("Ok")
  }
  
  private func tryCancelButton() {
    /* Given */
    app.buttons["Register"].tap()
    
    /* Then */
    cancelButton.tap()
  }
  
}
