//
//  ForgotPasswordTest.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/9/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class ForgotPasswordTest: BaseTest {
  
  lazy var submitButton = app.buttons["Send Password Reset Link"]
  lazy var cancelButton = app.buttons["Cancel"]

  lazy var emailField = app.textFields["Email"]
  
  override func setUp() {
    super.setUp()
    app.buttons["Forgot Password"].tap()
  }
  
  
  func tests() {
    tryBlankEmail()
    tryGoodEmail()
    tryCancelButton()
  }
  
  private func tryBlankEmail() {
    /* Given */
    emailField.replaceText("")
    submitButton.tap()

    /* Then */
    app.alerts["Blank Email"].waitForDialogAndTap("Close")
  }
  
  private func tryGoodEmail() {
    /* Given */
    emailField.replaceText(Config.email)
    submitButton.tap()
    submitButton.tap() // Needed to Pass Test?
    
    /* Then */
    app.alerts["Email Sent"].waitForDialogAndTap("Ok")
  }
  
  private func tryCancelButton() {
     /* Given */
    app.buttons["Forgot Password"].tap()
    
    /* Then */
    cancelButton.tap()
  }
  
}
