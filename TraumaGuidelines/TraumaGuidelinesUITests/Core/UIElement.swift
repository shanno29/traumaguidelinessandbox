//
//  UIElement.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/15/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

extension XCUIElement {
  
  func tapToolbarItem(_ text: String) {
    self.buttons[text].tap()
  }
  
  func replaceText(_ text: String) -> Void {
    self.tap()
    self.typeText(String(text.flatMap { _ in "\u{8}" }))
    self.typeText(text)
  }
  
  func waitForDialogAndTap(_ text: String) {
    let expectation = XCTNSPredicateExpectation(predicate: NSPredicate(format: "exists == true"), object: self)
    XCTAssert(XCTWaiter().wait(for: [expectation], timeout: 10) == .completed)
    
    let button = self.buttons[text]
    XCTAssert(button.exists)
    button.tap()
  }
  
}
