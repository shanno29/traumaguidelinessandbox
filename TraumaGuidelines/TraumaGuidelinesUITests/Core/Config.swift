//
//  Config.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/15/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import Foundation

struct Config {
  static let email = "mshannon93@gmail.com"
  static let name = "Matthew Shannon"
  static let password = "qqqqqq"

  static func randomString() -> String {
    return UUID().uuidString.replacingOccurrences(of: "-", with: "")
  }
  
}
