//
//  TraumaGuidelinesUITests.swift
//  TraumaGuidelinesUITests
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import XCTest

class BaseTest: XCTestCase {
  
  lazy var app = XCUIApplication()
  
  override func setUp() {
    super.setUp()
    continueAfterFailure = false
    app.launchArguments += ["UI-Testing"]
    app.launch()
  }
  
  override func tearDown() {
    super.tearDown()
    app.terminate()
  }
  
}

extension BaseTest {
  
  func loginUser() {
    app.textFields["Email"].replaceText(Config.email)
    app.secureTextFields["Password"].replaceText(Config.password)
    app.buttons["Login"].tap(); sleep(2)
  }
  
}
