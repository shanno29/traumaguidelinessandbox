//
//  ContentData.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

public var contentData = ContentData()


public class ContentData {
    
    var className: String = String(describing: Content.self)
    
    func list(sorted: Bool = true) -> [Content] {
        let fetchRequest:NSFetchRequest<Content> = Content.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        var results:[Content] = []
        do { results = try database.context.fetch(fetchRequest) }
        catch { print("error fetching content list")}
        if sorted { results = results.sorted { $0.id < $1.id }}
        return results
    }
    
    func insertOrUpdate(id: Int16, topicId: Int16, explanation: String) {
        if let content = find(id: id) {
            content.id = id
            content.topicId = topicId
            content.explanation = explanation
            _=database.save()
        } else {
            insert(id: id, topicId: topicId, explanation: explanation)
        }
    }
    
    func insert(id: Int16, topicId: Int16, explanation: String) {
        let res = NSEntityDescription.insertNewObject(forEntityName: className, into: database.context)
        guard let content = res as? Content else { return }
        content.id = id
        content.topicId = topicId
        content.explanation = explanation
        _=database.save()
    }
    
    
    func update(data: JSON) {
        for content in data {
            let id = content.1["id"].int16Value
            let topicId = content.1["topic_id"].int16Value
            let explanation = content.1["description"].stringValue
            insertOrUpdate(id: id, topicId: topicId, explanation: explanation)
        }
    }
    
    func delete(data: JSON) {
        for content in data {
            if let content = find(id: content.1["id"].int16Value) {
                _=database.delete(content)
            }
        }
    }
    
    
    func find(id: Int16) -> Content? {
        var content:Content?
        let fetchRequest: NSFetchRequest<Content> = Content.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format: "id == %@", argumentArray: [id])
        do { content = try database.context.fetch(fetchRequest).first }
        catch { print("error fetching content with id: \(id)")}
        return content
    }
    
     func findWhere(topicId: Int16) -> Content? {
        var content:Content?
        let fetchRequest:NSFetchRequest<Content> = Content.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format: "topicId == %@", argumentArray: [topicId])
        do { content = try database.context.fetch(fetchRequest).first }
        catch { print("error fetching contents with topicId \(topicId)") }
        return content
    }
    
}
