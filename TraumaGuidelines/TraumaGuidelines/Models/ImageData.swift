//
//  Image.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

public var imageData = ImageData()


public class ImageData {
    
    var className: String = String(describing: ContentImage.self)
    
    func list(sorted: Bool = true) -> [ContentImage] {
        let fetchRequest:NSFetchRequest<ContentImage> = ContentImage.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        var results:[ContentImage] = []
        do { results = try database.context.fetch(fetchRequest) }
        catch { print("error fetching image list")}
        if sorted { results = results.sorted { $0.id < $1.id }}
        return results
    }
    
    func insertOrUpdate(id: Int16, contentId: Int16, title: String, url: String) {
        if let image = find(id: id) {
            image.id = id
            image.contentId = contentId
            image.title = title
            image.url = url
            _=database.save()
        } else {
            insert(id: id, contentId: contentId, title: title, url: url)
        }
    }
    
    func insert(id: Int16, contentId: Int16, title: String, url: String) {
        let res = NSEntityDescription.insertNewObject(forEntityName: className, into: database.context)
        guard let image = res as? ContentImage else { return }
        image.id = id
        image.contentId = contentId
        image.title = title
        image.url = url
        _=database.save()
    }
    
    
    func update(data: JSON, _ completion: @escaping (Bool) -> Void) {
        var count = data.count
        
        if count == 0 {
            completion(true)
            return
        }
        
        var flag = false
        for image in data {
            let id = image.1["id"].int16Value
            let contentId = image.1["content_id"].int16Value
            let title = image.1["title"].stringValue
            let serverUrl = image.1["url"].stringValue
            var fileName = ""
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileDir = documentsURL.appendingPathComponent("imageFiles", isDirectory: true)
            fileName = "\(id).\(NSString(string: serverUrl).pathExtension)"
            let fileUrl = fileDir.appendingPathComponent(fileName)

            api.downloadFile(from: serverUrl, to: "images", named: title, withCallback: { (success) in
                count = count - 1
                if success {
                    self.insertOrUpdate(id: id, contentId: contentId, title: title, url: "\(fileUrl)")
                } else {
                    flag = true
                }
                if count == 0 {
                    completion(!flag)
                    return
                }
            })
            completion(!flag)
        }
    }
    
    func delete(data: JSON) {
        for image in data {
            if let image = find(id: image.1["id"].int16Value) {
                _=database.delete(image)
            }
        }
    }
    
    func find(id: Int16) -> ContentImage? {
        var image:ContentImage?
        let fetchRequest: NSFetchRequest<ContentImage> = ContentImage.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format: "id == %@", argumentArray: [id])
        do { image = try database.context.fetch(fetchRequest).first }
        catch { print("error fetching image with id: \(id)")}
        return image
    }
    
     func findWhere(contentId: Int16) -> [ContentImage] {
        var images:[ContentImage] = []
        let fetchRequest:NSFetchRequest<ContentImage> = ContentImage.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format: "contentId == %@", argumentArray: [contentId])
        do { images = try database.context.fetch(fetchRequest) }
        catch { print("error fetching images with contentId \(contentId)") }
        return images
    }
    
}
