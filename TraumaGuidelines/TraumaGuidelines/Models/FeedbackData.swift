//
//  FeedbackData.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/14/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

public var feedbackData = FeedbackData()


public class FeedbackData {
  
  var className: String = String(describing: Feedback.self)
  
  func list(sorted: Bool = true) -> [Feedback] {
    let fetchRequest:NSFetchRequest<Feedback> = Feedback.fetchRequest()
    fetchRequest.returnsObjectsAsFaults = false
    var results:[Feedback] = []
    do { results = try database.context.fetch(fetchRequest) }
    catch { print("error fetching Feedback list")}
    if sorted { results = results.sorted { $0.topicId < $1.topicId }}
    return results
  }
  
  func insertOrUpdate(id: String, topicId: Int16, reason: String, desc: String) {
    if let feedback = find(id: id) {
      feedback.id = id
      feedback.topicId = topicId
      feedback.reason = reason
      feedback.desc = desc
      _=database.save()
    } else {
      insert(id: id, topicId: topicId, reason: reason, desc: desc)
    }
  }
  
  func insert(id: String, topicId: Int16, reason: String, desc: String) {
    let res = NSEntityDescription.insertNewObject(forEntityName: className, into: database.context)
    guard let feedback = res as? Feedback else { return }
    feedback.id = id
    feedback.topicId = topicId
    feedback.reason = reason
    feedback.desc = desc
    _=database.save()
  }
  
  func update(data: JSON) {
    for feedback in data {
      let id = feedback.1["id"].stringValue
      let topicId = feedback.1["topicId"].int16Value
      let reason = feedback.1["reason"].stringValue
      let desc = feedback.1["desc"].stringValue
      insertOrUpdate(id: id, topicId: topicId, reason: reason, desc: desc)
    }
  }
  
  func delete(feedback: Feedback) {
    _=database.delete(feedback)
  }
  
  func find(id: String) -> Feedback? {
    var feedback:Feedback?
    let fetchRequest: NSFetchRequest<Feedback> = Feedback.fetchRequest()
    fetchRequest.returnsObjectsAsFaults = false
    fetchRequest.predicate = NSPredicate(format: "id == %@", argumentArray: [id])
    do { feedback = try database.context.fetch(fetchRequest).first }
    catch { print("error fetching feedback with id: \(id)")}
    return feedback
  }
  
  func findWhere(topicId: Int) -> [Feedback] {
    var feedbacks:[Feedback] = []
    let fetchRequest:NSFetchRequest<Feedback> = Feedback.fetchRequest()
    fetchRequest.predicate = NSPredicate(format: "parentId == %@", argumentArray: [topicId])
    fetchRequest.returnsObjectsAsFaults = false
    do { feedbacks = try database.context.fetch(fetchRequest) }
    catch { print("error fetching feedbacks with topic id \(topicId)") }
    return feedbacks
  }
  
}

