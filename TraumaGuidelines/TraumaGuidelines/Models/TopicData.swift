//
//  TopicData.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

public var topicData = TopicData()


public class TopicData {
    
    var className: String = String(describing: Topic.self)
    
    func list(sorted: Bool = true) -> [Topic] {
        let fetchRequest:NSFetchRequest<Topic> = Topic.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        var results:[Topic] = []
        do { results = try database.context.fetch(fetchRequest) }
        catch { print("error fetching Topic list")}
        if sorted { results = results.sorted { $0.id < $1.id }}
        return results
    }
    
    func insertOrUpdate(id: Int16, parentId: Int16, title: String, tags: String, summary: String) {
        if let topic = find(id: id) {
            topic.id = id
            topic.parentId = parentId
            topic.title = title
            topic.tags = tags
            topic.summary = summary
            _=database.save()
        } else {
            insert(id: id, parentId: parentId, title: title, tags: tags, summary: summary)
        }
    }
    
    func insert(id: Int16, parentId: Int16, title: String, tags: String, summary: String) {
        let res = NSEntityDescription.insertNewObject(forEntityName: className, into: database.context)
        guard let topic = res as? Topic else { return }
        topic.id = id
        topic.parentId = parentId
        topic.title = title
        topic.tags = tags
        topic.summary = summary
        _=database.save()
    }
    
    func update(data: JSON) {
        for topic in data {
            let id = topic.1["id"].int16Value
            let parentId = topic.1["parent_id"].int16Value
            let title = topic.1["title"].stringValue
            let tags = topic.1["tags"].stringValue
            let summary = topic.1["summary"].stringValue
            insertOrUpdate(id: id, parentId: parentId, title: title, tags: tags, summary: summary)
        }
    }
    
    func delete(data: JSON) {
        for topic in data {
            if let topic = find(id: topic.1["id"].int16Value) {
                _=database.delete(topic)
            }
        }
    }
    
    func find(id: Int16) -> Topic? {
        var topic:Topic?
        let fetchRequest: NSFetchRequest<Topic> = Topic.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format: "id == %@", argumentArray: [id])
        do { topic = try database.context.fetch(fetchRequest).first }
        catch { print("error fetching topic with id: \(id)")}
        return topic
    }
    
    func findWhere(parentId: Int) -> [Topic] {
        var topics:[Topic] = []
        let fetchRequest:NSFetchRequest<Topic> = Topic.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "parentId == %@", argumentArray: [parentId])
        fetchRequest.returnsObjectsAsFaults = false
        do { topics = try database.context.fetch(fetchRequest) }
        catch { print("error fetching topics with parent id \(parentId)") }
        return topics 
    }
    
    func findWhere(tag: String) -> [(Topic, String, String)] {
        var topics:[Topic] = []
        var returnTopics: [(Topic, String, String)] = []
        let fetchRequest:NSFetchRequest<Topic> = Topic.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "(tags CONTAINS[cd] %@) OR (summary CONTAINS[cd] %@) OR (title CONTAINS[cd] %@)", argumentArray: [tag, tag, tag])
        fetchRequest.returnsObjectsAsFaults = false
        do { topics = try database.context.fetch(fetchRequest)}
        catch { print("error fetching topics with tag \(tag)") }
        for topic in topics  {
            var foundIn = ""
            if topic.title?.lowercased().range(of: tag.lowercased()) != nil {
                if foundIn == "" {
                    foundIn += "Title"
                } else {
                    foundIn += ", Title"
                }
            }
            if topic.summary?.lowercased().range(of: tag.lowercased()) != nil {
                if foundIn == "" {
                    foundIn += "Summary"
                } else {
                    foundIn += ", Summary"
                }
            }
            if topic.tags?.lowercased().range(of: tag.lowercased()) != nil {
                if foundIn == "" {
                    foundIn += "Tags"
                } else {
                    foundIn += ", Tags"
                }
            }
            
            var parent = find(id: topic.parentId)
            var count = 0
            
            var pathTopics: [Topic] = []
            while parent != nil && count < 2 {
                pathTopics.append(parent!)
                count += 1
                parent = find(id: parent!.parentId)
            }
            
            var pathString = ""
            for pathTopic in pathTopics.reversed() {
                if pathString.isEmpty {
                    pathString += pathTopic.title ?? ""
                } else {
                    pathString += " > \(pathTopic.title ?? "")"
                }
            }

            returnTopics.append((topic, foundIn, pathString))
        }
        return returnTopics
    }
    
    func findWhereWithin(topic: Topic, tag: String)  -> [(Topic, String, String)] {
        var topics: [(Topic, String, String)] = []
        for childTopic in findWhere(parentId: Int(topic.id)) {
            var contains = false
            var foundIn = ""
            if childTopic.title?.lowercased().range(of: tag.lowercased()) != nil {
                contains = true
                if foundIn == "" {
                    foundIn += "Title"
                } else {
                    foundIn += ", Title"
                }
            }
            if childTopic.summary?.lowercased().range(of: tag.lowercased()) != nil {
                contains = true
                if foundIn == "" {
                    foundIn += "Summary"
                } else {
                    foundIn += ", Summary"
                }
            }
            if childTopic.tags?.lowercased().range(of: tag.lowercased()) != nil {
                contains = true
                if foundIn == "" {
                    foundIn += "Tags"
                } else {
                    foundIn += ", Tags"
                }
            }
            
            var parent = find(id: topic.parentId)
            var count = 0
            
            var pathTopics: [Topic] = []
            while parent != nil && count < 2 {
                pathTopics.append(parent!)
                count += 1
                parent = find(id: parent!.parentId)
            }
            
            var pathString = ""
            for pathTopic in pathTopics.reversed() {
                if pathString.isEmpty {
                    pathString += pathTopic.title ?? ""
                } else {
                    pathString += " > \(pathTopic.title ?? "")"
                }
            }


            if contains { topics.append((childTopic, foundIn, pathString))}
            topics.append(contentsOf: findWhereWithin(topic: childTopic, tag: tag))
        }
        return topics
    }
    
}

