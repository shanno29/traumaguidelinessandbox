//
//  API.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

typealias NetworkResult = (Bool, String, String) -> Void

public let api = API()

public class API {
    var baseURL = "https://appbrewerydev.uwm.edu/trauma-guidelines/api/v1"
  
}

extension API {
    
    
    //MARK: Registration
    func register(name: String, email: String, _ completion: @escaping NetworkResult) {
        let requestURL = "\(baseURL)/register"
        let parameters : Parameters = [
            "name" : name,
            "email" : email
        ]
        Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            if response.result.isFailure {
                completion(false, "Registration Error", "There was an error with the request")
                print("API call failed for \(requestURL) to register new user")
                return
            }
            print("API call succeeded for \(requestURL) to register new user")
            if let anyJSON = response.result.value {
                let responseJSON = JSON(anyJSON)
                
                if responseJSON["meta", "success"].boolValue {
                    let title = responseJSON["data","title"].stringValue
                    let message = responseJSON["data", "message"].stringValue
                    completion(true, title, message)
                } else {
                    let title = responseJSON["data","title"].stringValue
                    let message = responseJSON["data", "message"].stringValue
                    completion(false, title, message)
                }
            }
        }
    }
    
    
    //MARK: Authentication WITHOUT api token
    func authenticate(email: String, password: String, _ completion: @escaping NetworkResult) {
        let requestURL = "\(baseURL)/authenticate"
        let parameters : Parameters = [
            "email" : email,
            "password" : password
        ]
        
        Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            if response.result.isFailure {
                completion(false, "Registration Error", "There was an error with the request")
                print("API call failed for \(requestURL) to authenticate user")
                return
            }
            print("API call succeeded for \(requestURL) to authenticate user")
            if let anyJSON = response.result.value {
                let responseJSON = JSON(anyJSON)
                if responseJSON["meta", "success"].boolValue {
                    Prefs.set(entry: .userID, value: responseJSON["data","user","id"].stringValue)
                    Prefs.set(entry: .userToken, value: responseJSON["data","user","api_token"].stringValue)
                    Prefs.set(entry: .userName, value: responseJSON["data","user","name"].stringValue)
                    Prefs.set(entry: .userEmail, value: responseJSON["data","user","email"].stringValue)
                    completion(true, "", "")
                } else {
                    completion(false, responseJSON["data","title"].stringValue, responseJSON["data","message"].stringValue)
                }
            }
        }
 
    }
    
    //MARK: Authentication WITH api token
    func authenticate(apiToken: String, _ completion: @escaping NetworkResult) {
        let requestURL = "\(baseURL)/authenticate"
        let parameters : Parameters = [
            "api_token": apiToken
        ]
        
        Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            if response.result.isFailure {
                completion(false, "Registration Error", "There was an error with the request")
                print("API call failed for \(requestURL) to authenticate user")
                return
            }
            print("API call succeeded for \(requestURL) to authenticate user")
            if let anyJSON = response.result.value {
                let responseJSON = JSON(anyJSON)
                if responseJSON["meta", "success"].boolValue {
                    Prefs.set(entry: .userID, value: responseJSON["data","user","id"].stringValue)
                    Prefs.set(entry: .userToken, value: responseJSON["data","user","api_token"].stringValue)
                    Prefs.set(entry: .userName, value: responseJSON["data","user","name"].stringValue)
                    Prefs.set(entry: .userEmail, value: responseJSON["data","user","email"].stringValue)
                    completion(true, "", "")
                } else {
                    completion(false, responseJSON["data","title"].stringValue, responseJSON["data","message"].stringValue)
                }
            }
        }
    }
    
    
    //MARK: Forgot Password
    func forgotPassword(email: String, _ completion: @escaping NetworkResult) {
        let requestURL = "\(baseURL)/forgot"
        let parameters : Parameters = [
            "email" : email
        ]
        Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (response) in
            if response.result.isFailure {
                completion(false, "Error", "There was an error with the request")
                print("API call failed for \(requestURL) to send forgot password email")
                return
            }
            print("API call succeeded for \(requestURL) to send forgot password email")
            if let anyJSON = response.result.value {
                let responseJSON = JSON(anyJSON)
                completion(responseJSON["meta","success"].boolValue, responseJSON["data","title"].stringValue, responseJSON["data","message"].stringValue)
                return
            }
        }
    }
    
    //MARK: Batch Update
    func batchContent(_ completion: @escaping NetworkResult) {
        guard let apiToken = Prefs.get(entry: .userToken) as? String else {
            completion(false, "Error", "Please try again later")
            return
            
        }
        
        let iso8601: DateFormatter = {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
            return formatter
        }()
        var requestURL = "\(baseURL)/batch"
        if let updatedSince = Prefs.get(entry: .contentUpdatedAt) as? Date {
            let isoString = iso8601.string(from: updatedSince)
            requestURL = "\(requestURL)?since=\(isoString)"
        }
        
        let headers: [String: String] = [
            "Authorization" : "Bearer \(apiToken)",
            "Accept": "application/json"
        ]
        
        Alamofire.request(requestURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isFailure {
                completion(false, "Content Update Error", "There was an error with the request")
                print("API call failed for \(requestURL) to update content")
                return
            }
            print("API call succeeded for \(requestURL) to update content")
            if let anyJSON = response.result.value {
                let responseJSON = JSON(anyJSON)
                if responseJSON["meta", "success"].boolValue {
                    topicData.update(data: responseJSON["data", "topics"])
                    topicData.delete(data: responseJSON["data", "deleted", "topics"])
                    
                    contentData.update(data: responseJSON["data", "contents"])
                    contentData.delete(data: responseJSON["data", "deleted", "contents"])
                
                    imageData.delete(data: responseJSON["data", "deleted", "topics"])
                    imageData.update(data: responseJSON["data", "images"],{ (success) in
                        if success {
                            Prefs.set(entry: .contentUpdatedAt, value: Date())
                        }
                        completion(true, "", "")
                    })
                    
                } else {
                    completion(false, responseJSON["data","title"].stringValue, responseJSON["data","message"].stringValue)
                }
            } else {
                completion(false, "Content Update Error", "There was an error with the request")
            }
        }
    }
    
    
    //MARK: Download file
    func downloadFile(from sourceURL: String, to directory: String, named name: String, withCallback completion: @escaping (Bool) -> Void ) {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileDir = documentsURL.appendingPathComponent(directory, isDirectory: true)
        let fileName = "\(name).\(NSString(string: sourceURL).pathExtension)"
        let fileURL = fileDir.appendingPathComponent(fileName)

        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        Alamofire.download(sourceURL, to: destination).responseData { response in
            if !response.result.isSuccess {
                completion(false)
                return
            }
            if let data = response.result.value {
                do {
                    try data.write(to: fileURL)
                    print("Successfully downloaded file to \"\(fileURL)\" directory.")
                    completion(true)
                } catch let error {
                    completion(false)
                }
            }
        }
    }
  
  //MARK: Feedback
  func postFeedback(feedback: Feedback, _ completion: @escaping NetworkResult) {
      guard let apiToken = Prefs.get(entry: .userToken) as? String else { completion(false, "", ""); return }
      let headers: [String: String] = [
        "Authorization" : "Bearer \(apiToken)",
        "Accept": "application/json"
      ]
   
      let requestURL = "\(baseURL)/feedback"
      let parameters : Parameters = [
          "topic_id" : feedback.topicId,
          "reason" : feedback.reason ?? "",
          "description": feedback.desc ?? ""
      ]
      print(parameters)
    
      Alamofire.request(requestURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        if response.result.isFailure {
          completion(false, "Error", "There was an error with the request")
          print("API call failed for \(requestURL) to send feedback")
          return
        }
        print("API call succeeded for \(requestURL) t send feedback")
        if let anyJSON = response.result.value {
          let responseJSON = JSON(anyJSON)
          completion(responseJSON["meta","success"].boolValue, responseJSON["data","title"].stringValue, responseJSON["data","message"].stringValue)
          return
        }
      }
  }
  
  
}
