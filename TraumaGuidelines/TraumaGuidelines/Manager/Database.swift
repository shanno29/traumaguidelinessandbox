//
//  Database.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import Foundation
import CoreData

public var database = Database()

public class Database {
    
    private  var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TraumaGuidelines")
        container.loadPersistentStores { storeDescription, error in
            if let error = error { print(error) }
        }
        return container
    }()
    
    var context: NSManagedObjectContext { return self.persistentContainer.viewContext }
    
    func save() -> Bool {
        if context.hasChanges {
            do {
                try context.save()
                return true
            }
            catch { print("Saving error:", error.localizedDescription) }
            return false
        }
        return true
    }
    
    func delete(_ object: NSManagedObject) -> Bool {
        context.delete(object)
        return save()
    }
}
