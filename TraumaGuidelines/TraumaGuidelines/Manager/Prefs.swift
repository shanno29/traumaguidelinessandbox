//
//  UserDefaults.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import Foundation

public let Prefs = Defaults()

public class Defaults {
    
    func set(entry: Entries, value: Any) {
        switch entry {
        case .userToken:
            if let userToken = value as? String {
                UserDefaults.standard.set(userToken, forKey: "userToken")
            }
        case .userID:
            if let id = value as? String {
                UserDefaults.standard.set(id, forKey: "userID")
            }
        case .contentUpdatedAt:
            if let date = value as? Date {
                UserDefaults.standard.set(date, forKey: "contentUpdatedAt")
            }
        case .userEmail:
            if let email = value as? String {
                UserDefaults.standard.set(email, forKey: "userEmail")
            }
        case .userName:
            if let name = value as? String {
                UserDefaults.standard.set(name, forKey: "userName")
            }
        case .searchAllTopics:
            if let bool = value as? Bool {
                UserDefaults.standard.set(bool, forKey: "searchAllTopics")
            }
        default:
            return
        }
       
    }
    
    func get(entry: Entries) -> Any {
        switch entry {
        case .userToken:
            return UserDefaults.standard.string(forKey: "userToken") as Any
        case .userID:
            return UserDefaults.standard.string(forKey: "userID") as Any
        case .contentUpdatedAt:
            return UserDefaults.standard.object(forKey: "contentUpdatedAt") as Any
        
        case .contentUpdatedAtString:
            let date = UserDefaults.standard.object(forKey: "contentUpdatedAt") as! Date
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd, yyyy 'at' h:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            let dateString = formatter.string(from: date)
            return dateString
            
        case .userName:
            return UserDefaults.standard.string(forKey: "userName") as Any
        case .userEmail:
            return UserDefaults.standard.string(forKey: "userEmail") as Any
        case .searchAllTopics:
            return UserDefaults.standard.bool(forKey: "searchAllTopics") as Any
        }
        
    }
    
    func remove(entry: Entries) {
        var key = ""
        switch entry {
        case .userToken:
            key = "userToken"
        case .userID:
            key = "userID"
        case .contentUpdatedAt:
            key = "contentUpdatedAt"
        case .userName:
            key = "userName"
        case .userEmail:
            key = "userEmail"
        case .searchAllTopics:
            key = "searchAllTopics"
        default:
            return
        }
        UserDefaults.standard.removeObject(forKey: key)
    }
}

enum Entries {
    case userToken, userID, contentUpdatedAt, contentUpdatedAtString, userName, userEmail, searchAllTopics
}
