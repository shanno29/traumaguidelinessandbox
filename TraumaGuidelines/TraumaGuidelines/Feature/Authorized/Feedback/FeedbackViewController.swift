//
//  FeedbackViewController.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/7/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class FeedbackViewController: BaseVC, FeedbackView {
  
  @IBOutlet weak var selectReasonLabelHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var selectionTableHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var inputFieldHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var selectionTable: UITableView!
  @IBOutlet weak var selectReasonLabel: UILabel!
  @IBOutlet weak var inputField: UITextView!
    
  var presenter: FeedbackPresenter!
  
  static func setup(_ topic: Topic?) -> FeedbackViewController {
    let vc = FeedbackViewController.inflate()
    vc.presenter = FeedbackPresenter(view: vc, topic)
    vc.title = vc.presenter.topic?.title ?? ""
    return vc
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    keyboard.delegate = self
    inputField.delegate = self
    selectionTable.delegate = self
    selectionTable.dataSource = self
    navigationItem.leftBarButtonItem = backBarButton
  }
  
}

//MARK: Events
extension FeedbackViewController {
  
  @IBAction func onSubmitClicked() { presenter.validateInput(inputField.text) }
  
  @IBAction func onCancelClicked() { presenter.onCancel() }

}

//MARK: BackButton
extension FeedbackViewController {

  var backBarButton: UIBarButtonItem { get {
      return UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backButtonTapped))
  }}
  
  @objc func backButtonTapped() { presenter.onCancel() }
  
}

// MARK: UITableViewDataSource
extension FeedbackViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return CheckboxCell.height }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return presenter.data.items.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CheckboxCell.id, for: indexPath) as! CheckboxCell
        return cell.setup(presenter.data.items[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.lastSelection = indexPath.row
        
        for pos in 0..<presenter.data.items.count {
            let cell = tableView.cellForRow(at: IndexPath(row: pos, section: 0)) as! CheckboxCell
            cell.onClick(pos == presenter.lastSelection)
        }
    }
}

//MARK: Keyboard
extension FeedbackViewController: KeyboardDelegate {
    
    func onKeyboardShown() {
        selectReasonLabelHeightConstraint.constant = 0
        selectionTableHeightConstraint!.constant = 0
        inputFieldHeightConstraint!.constant = 200
        selectReasonLabel.alpha = 0.0
        selectionTable.alpha = 0.0
    }
    
    func onKeyboardHidden() {
        selectReasonLabelHeightConstraint.constant = 20.5
        selectionTableHeightConstraint!.constant = 200
        inputFieldHeightConstraint!.constant = 100
        selectReasonLabel.alpha = 1.0
        selectionTable.alpha = 1.0
    }
    
}

