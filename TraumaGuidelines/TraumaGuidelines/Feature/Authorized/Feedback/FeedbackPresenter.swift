//
//  FeedbackPresenter.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol FeedbackView: BaseView {
    //func goToHome()
    //func goToLogin()
}

class FeedbackPresenter {
    
    let view: FeedbackView
    var lastSelection: Int = -1
    let topic: Topic?
    
    init(view: FeedbackView, _ topic: Topic?) {
        self.view = view
        self.topic = topic
    }
    
    let data = FeedbackSelection("Select a Reason:", [
        "Spelling Error",
        "Incorrect",
        "Suggestion",
        "Other"
    ])
    
    func validateInput(_ desc: String?) {
        let reason = lastSelection > -1 ? data.items[lastSelection] : ""
        let topicId = topic?.id ?? 0
        let _desc = desc ?? ""

        if reason.isEmpty { view.showError("Blank Reason", "Please select a Reason"); return }
        if _desc.isEmpty { view.showError("Blank Description", "Please enter your Description"); return }
        sendFeedback(topicId, reason, _desc)
    }
    
    func sendFeedback(_ topicId: Int16, _ reason: String, _ desc: String) {
        let id = "\(feedbackData.list().count+1)"
        feedbackData.insertOrUpdate(id: id, topicId: topicId, reason: reason, desc: desc)
        onCancel()
    }
    
    func onCancel() { view.goBack() }

}
