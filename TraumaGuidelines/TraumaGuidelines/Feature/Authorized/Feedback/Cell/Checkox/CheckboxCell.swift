//
//  CheckboxCell.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/13/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class CheckboxCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var checkbox: Checkbox!
  static let height: CGFloat = 50
  static var id = "CheckboxCell"
  
  func setup(_ text: String) -> Self {
    titleLabel.text = text
    selectionStyle = .none
    return self
  }
  
  func onClick(_ state: Bool) {
    checkbox.isUserInteractionEnabled = false
    checkbox.onClick(state)
  }
  
}
