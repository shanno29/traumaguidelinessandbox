//
//  Checkbox.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/8/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class Checkbox: UIView {

  @IBOutlet var contentView: UIView!
  @IBOutlet weak var outerLayer: UIView!
  @IBOutlet weak var innerLayer: UIView!
  var unselectedColor = UIColor(r: 18, g: 107, b: 173)
  var selectedColor = UIColor(r: 214, g: 214, b: 214)

  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    Bundle.main.loadNibNamed("Checkbox", owner: self, options: nil)
    addSubview(contentView)
    contentView.frame = bounds
    contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    outerLayer.layer.cornerRadius = cornerRadius
    innerLayer.layer.cornerRadius = cornerRadius
  }
  
  @IBAction func onClick(_ state: Bool) {
    outerLayer.backgroundColor = state ? selectedColor: unselectedColor
    innerLayer.backgroundColor = state ? unselectedColor: selectedColor
  }
  
}
