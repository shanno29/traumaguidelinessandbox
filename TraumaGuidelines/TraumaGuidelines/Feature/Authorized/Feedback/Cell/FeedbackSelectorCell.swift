//
//  FeedbackSelectorCell.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/13/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class FeedbackSelectorCell: UITableViewCell {
  
  @IBOutlet weak var selectionsTable: UITableView!
  @IBOutlet weak var titleLabel: UILabel!
  static var id = "FeedbackSelectorCell"
  var data: FeedbackSelection!
  
  func setup(_ data: FeedbackSelection) -> Self {
    self.data = data;
    titleLabel.text = data.title
    return self
  }
  
}

class FeedbackSelection {
  
  let title: String
  let items: [String]
  
  
  init(_ title: String, _ items: [String]) {
    self.title = title
    self.items = items
  }
  
}
