//
//  FeedbackPresenter.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol ContentView: BaseView {
    //func goToHome()
    //func goToLogin()
}

class ContentPresenter {
    
    let view: ContentView
    
    var topic: Topic?
    var content: Content?
    var images: [ContentImage] = []
    var webViewHeight: CGFloat = 0.0
    var currentPage = 0

    
    init(view: ContentView, _ topic: Topic?, _ content: Content) {
        self.view = view
        self.topic = topic
        self.content = content
        self.images = imageData.findWhere(contentId: content.id)
    }
    
    func currentIndex(_ index: Int) -> Int {
        switch index{
            case 0: if currentPage != 0 { currentPage -= 1 }
            case 1: if currentPage != images.count { currentPage += 1 }
            default: return currentPage
        }
        return currentPage
    }
    
    func onCancel() { view.goBack() }
    
}

