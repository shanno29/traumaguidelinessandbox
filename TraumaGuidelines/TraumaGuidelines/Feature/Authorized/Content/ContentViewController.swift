//
//  ContentViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/7/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit
import WebKit

class ContentViewController: BaseVC, WKNavigationDelegate, ContentView {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backPageButton: UIButton!
    @IBOutlet weak var nextPageButton: UIButton!
    
    var presenter: ContentPresenter!
    
    static func setup(_ content: Content, _ topic: Topic) -> ContentViewController {
        let vc = ContentViewController.inflate()
        vc.presenter = ContentPresenter(view: vc, topic, content)
        vc.title = topic.title
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = []
        updatePageButtonVisibilities()

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false

        navigationItem.leftBarButtonItem = backButton
        navigationItem.rightBarButtonItem = feedbackBarButton
  }
  
}

//MARK IBAction outlets
extension ContentViewController {
    
    @IBAction func pageNavigationButtonTapped(_ sender: UIButton) {
        let index = IndexPath(item: presenter.currentIndex(sender.tag), section: 0)
        collectionView.scrollToItem(at: index, at: .right, animated: true)
        updatePageButtonVisibilities()
    }
    
}

//Mark: Helper functions
extension ContentViewController {
    
    func updatePageButtonVisibilities() {
        pageControl.numberOfPages = presenter.images.count + 1
        pageControl.currentPage = presenter.currentPage
        backPageButton.isHidden = presenter.currentPage == 0
        nextPageButton.isHidden = presenter.currentPage == presenter.images.count
    }
  
}

//MARK: FeedbackButton
extension ContentViewController {
    
    var feedbackBarButton: UIBarButtonItem { get {
        return UIBarButtonItem(image: UIImage(named: "feedback"), style: .plain, target: self, action: #selector(feedbackOnClick))
    }}
    
    @objc func feedbackOnClick() { router.goTo(FeedbackViewController.setup(presenter.topic)) }
    
}

//MARK: Bar Button Functions
extension ContentViewController {

    var backButton: UIBarButtonItem { get {
        return UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backButtonTapped))
    }}
  
    @objc func backButtonTapped() { router.goBack() }

}

//MARK: UICollectionView Data Source and Delegate methods
extension ContentViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return presenter.images.count + 1 }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat { return 0 }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        presenter.currentPage = Int(targetContentOffset.pointee.x / view.frame.width)
        updatePageButtonVisibilities()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "webViewCell", for: indexPath) as! WebViewCollectionViewCell
            return cell.setup(presenter.content!.explanation!)
        }
            
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageViewCell", for: indexPath) as! ImageCollectionViewCell
            return cell.setup(presenter.images[indexPath.row - 1])
        }
        
    }
    
}
