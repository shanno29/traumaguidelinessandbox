//
//  WebViewCollectionViewCell.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/7/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit
import WebKit

class WebViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var webView: WKWebView!
    
    func setup(_ content: String) -> Self {
        webView.translatesAutoresizingMaskIntoConstraints = false
        let style = "<style>* { font-family: sans-serif; }</style>"
        let meta = "<meta name='viewport' content='user-scalable=no, initial-scale=1'>"
        let html = "<html> <head>\(meta)\(style)</head> <body>\(content)</body> </html>"
        webView.loadHTMLString(html, baseURL: nil)
        return self
    }
    
}
