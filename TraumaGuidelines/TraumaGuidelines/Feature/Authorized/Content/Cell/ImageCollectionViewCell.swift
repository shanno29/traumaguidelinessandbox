//
//  ImageCollectionViewCell.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/7/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var title: UILabel!
    
    func setup(_ image: ContentImage) -> Self {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileDir = documentsURL.appendingPathComponent("images", isDirectory: true)
        let fileURL = fileDir.appendingPathComponent(image.title ?? "")
        imageView.image = UIImage(contentsOfFile: fileURL.path)
        title.text = image.title ?? ""
        imageView.clipsToBounds = true
        return self
    }
    
}
