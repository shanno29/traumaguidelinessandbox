//
//  SettingsViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/14/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class SettingsViewController: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    
    var sections: [String] = ["User", "Content", "Search"]
    
    var userInfo: [String: String] = ["Name": "Chase", "Email" : "lettene3@Uwm.edu"]
  
    static func setup() -> SettingsViewController {
        let vc = SettingsViewController.inflate()
        vc.modalTransitionStyle = .flipHorizontal
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
      
        navigationItem.leftBarButtonItem = backBarButton
        navigationItem.rightBarButtonItem = signOutButton
        
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: CGFloat(40)))
        tableView.contentInset = UIEdgeInsetsMake(-CGFloat(40), 0, 0, 0)
        
        userInfo["Name"] = (Prefs.get(entry: .userName) as! String)
        userInfo["Email"] = (Prefs.get(entry: .userEmail) as! String)
    }
}


//MARK: Helper functions
extension SettingsViewController {
  
  func updateContent() {
    api.batchContent(onResponse(
        doOnSuccess:  { self.tableView.reloadSections(NSIndexSet(index: 1) as IndexSet, with: .left) },
        doOnError: showError
        )
    )
  }
  
}

//MARK: SignoutButton
extension SettingsViewController {
  
  var signOutButton: UIBarButtonItem { get {
      let title = "Sign Out"
      let action =  #selector(signOutButtonTapped)
    return UIBarButtonItem(title: title, style: .plain, target: self, action: action)
  }}
  
  @objc func signOutButtonTapped() {
    Prefs.remove(entry: .userToken)
    Prefs.remove(entry: .userID)
    Prefs.remove(entry: .userName)
    Prefs.remove(entry: .userEmail)
    self.present(LoginViewController.setup(), animated: true, completion: nil)
  }
  
}

//MARK: BackButton
extension SettingsViewController {
  
    var backBarButton: UIBarButtonItem { get {
        let image = UIImage(named: "back")
        let action =  #selector(backButtonTapped)
        return UIBarButtonItem(image: image, style: .plain, target: self, action: action)
    }}
  
    @objc func backButtonTapped() {
        self.router.goBack()
    }
  
    @objc func switchChanged(_ sender: UISwitch) {
        Prefs.set(entry: .searchAllTopics, value: sender.isOn)
    }
  
}

//MARK: UITableView Delegate, Data Source  and Helper methods
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource  {
  
    func numberOfSections(in tableView: UITableView) -> Int { return sections.count }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { return 50 }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? { return headerView(title: sections[section]) }
    
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      switch section {
        case 0: return userInfo.count
        case 1: return 1
        case 2: return 1
        default: return 0
      }
    }
  
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
          case 1: return 30
          case 2: return 50
          default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
          case 1: return footerView(title: "Last updated: \(Prefs.get(entry: .contentUpdatedAtString) as! String)")
          case 2: return footerView(title: "Toggle on to make search scan all topics. Otherwise search will only scan within current topic.")
          default: return  UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
          case 0: return userInfoCell(indexPath: indexPath)
          case 1: return contentCell(indexPath: indexPath)
          case 2: return searchTopicCell(indexPath: indexPath)
          default: return UITableViewCell()
        }
    }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: false)
      if indexPath.section == 1 { updateContent() }
  }
    
    func searchTopicCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.textLabel?.text = "Search All Topics"
        cell.selectionStyle = .none
        let switchView = UISwitch(frame: .zero)
        switchView.setOn(Prefs.get(entry: .searchAllTopics) as? Bool ?? false, animated: true)
        switchView.tag = indexPath.row // for detect which row switch Changed
        switchView.addTarget(self, action: #selector(switchChanged(_:)), for: .valueChanged)
        cell.accessoryView = switchView
        return cell
    }
    
    func userInfoCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        cell.textLabel?.text = Array(self.userInfo)[indexPath.row].key
        cell.selectionStyle = .none
        cell.detailTextLabel?.text = Array(self.userInfo)[indexPath.row].value
        cell.detailTextLabel?.textColor = UIColor.lightGray
        return cell
    }
    
    func contentCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = "Update Content"
        cell.selectionStyle = .gray
        cell.textLabel?.textColor = UIColor.blue
        return cell
    }
    
    func headerView(title: String) -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 100)

        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 24)
        titleLabel.backgroundColor = UIColor.clear
        
        view.addSubview(titleLabel)
        titleLabel.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 0, heightConstant: 0)

        return view
    }
    
    func footerView(title: String) -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 100)
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.lightGray
        titleLabel.font = UIFont.systemFont(ofSize: 12.0)
        titleLabel.numberOfLines = 3
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.backgroundColor = UIColor.clear
        view.addSubview(titleLabel)
        titleLabel.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        return view
    }
  
}
