//
//  HomeViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit
import AudioToolbox

class HomeViewController: BaseVC, HomeView {

    @IBOutlet weak var pathViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pathView: UICollectionView!
    @IBOutlet weak var buttonToolBar: UIToolbar!
    @IBOutlet weak var tableView: UITableView!
    var presenter: HomePresenter!
    
    static func setup() -> HomeViewController {
        let vc = HomeViewController.inflate()
        vc.modalTransitionStyle = .flipHorizontal
        vc.presenter = HomePresenter(view: vc)
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        pathView.dataSource = self
        pathView.delegate = self
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.postFeedbackIfNeeded()
        updateBarItems()
    }
  
}

//MARK: Nav
extension HomeViewController {
    
    func goToContent(_ content: Content, _ topic: Topic) { router.goTo(ContentViewController.setup(content, topic)) }

    func playSystemSound() { AudioServicesPlaySystemSound(1520) }

    func reloadTableSection(_ index: Int, slideIn: Bool) {
        let setIndex = NSIndexSet(index: index) as IndexSet
        tableView.reloadSections(setIndex, with: (slideIn ? .left : .right))
    }
    
    func updateBarItems () {
        
        let hasCurrentTopic = presenter.currentTopic == nil
        title = hasCurrentTopic ? presenter.currentTopic?.title : "Topics"
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let items = hasCurrentTopic ? [settingsBtn, space, searchBtn] : [settingsBtn, space, feedbackBarButton, space, searchBtn]
        
        buttonToolBar.setItems(items, animated: false)
        navigationItem.leftBarButtonItems = hasCurrentTopic ? [] : [backBtn]
        navigationItem.rightBarButtonItems = hasCurrentTopic ? [] : [homeButton]
        
        pathViewHeightConstraint.constant = hasCurrentTopic ? 0 : 44
        pathView.collectionViewLayout.invalidateLayout()
        
        animateBar()
    }
    
}

//MARK: UICollection View Methods
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { return presenter.topicsCount() }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.row
        if index % 2 == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "titleCell", for: indexPath) as! TitleLabelCollectionViewCell
            cell.title.font = UIFont.boldSystemFont(ofSize: 15)
            cell.title.text = presenter.pathTopics[Int(floor(Double(index / 2)))].title
            return cell
        } else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: "arrowCell", for: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat { return 0 }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row
        if index % 2 == 0 {
            let topic = presenter.pathTopics[Int(floor(Double(index / 2)))]
            if topic == presenter.currentTopic { return }
            presenter.currentTopic = topic
            presenter.currentTopics = topicData.findWhere(parentId: Int(topic.id))
            let set = NSIndexSet(index: 0)
            self.tableView.reloadSections(set as IndexSet, with: .right)
            self.updateBarItems()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let index = indexPath.row
        
        if index % 2 == 0 {
            let string =  presenter.pathTopics[Int(floor(Double(index / 2)))].title ??  ""
            let font = UIFont.boldSystemFont(ofSize: 15)
            let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: collectionView.frame.height)
            let boundingBox = string.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
            return CGSize(width: boundingBox.width + 10.0, height: collectionView.frame.height)
        } else {
            return CGSize(width: 25.0, height: collectionView.frame.height)
        }
    }

}

//MARK : TableView Delegate and Data Source Methods
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return presenter.currentTopics.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        return cell.setup(presenter.currentTopics[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.handleSelectingTopic(indexPath.row, false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let summary = presenter.currentTopics[indexPath.row].summary ?? ""
        let frame = CGSize(width: view.frame.width, height: 1000)
        let options = NSStringDrawingOptions.usesLineFragmentOrigin
        let attributes = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12)]
        let bounds = NSString(string: summary).boundingRect(with: frame, options: options, attributes: attributes, context: nil)
        return bounds.height + 40.0
    }
    
}

//MARK: FeedbackButton
extension HomeViewController {
    
    var feedbackBarButton: UIBarButtonItem { get { return UIBarButtonItem(image: UIImage(named: "feedback"), style: .plain, target: self, action: #selector(feedbackOnClick)) } }
    
    @objc func feedbackOnClick() { router.goTo(FeedbackViewController.setup(presenter.currentTopic)) }
    
}

//MARK: SettingsButton
extension HomeViewController {
    
    var settingsBtn: UIBarButtonItem { get { return UIBarButtonItem(image: UIImage(named: "settings"), style: .plain, target: self, action: #selector(settingsOnClick)) } }
    
    @objc func settingsOnClick() { router.goTo(SettingsViewController.setup()) }
    
}

//MARK: BackButton
extension HomeViewController {
    
    var backBtn: UIBarButtonItem { get { return UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backOnClick)) } }
    
    @objc func backOnClick() {
        title = presenter.updateTopic()
        reloadTableSection(0, slideIn: false)
        updateBarItems()
    }
}

//MARK: Home Buttton
extension HomeViewController {
    
    var homeButton : UIBarButtonItem { get { return UIBarButtonItem(image: UIImage(named: "home"), style: .plain, target: self, action: #selector(homeButtonTapped)) } }
    
    @objc func homeButtonTapped() {
        title = presenter.setParentTopic()
        reloadTableSection(0, slideIn: false)
        updateBarItems()
    }
    
}

//MARK: Search Functionality
extension HomeViewController {
    
    var searchBtn: UIBarButtonItem { get { return UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(searchButtonTapped)) } }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        let vc = SearchViewController.setup(presenter)
        let dest = UINavigationController(rootViewController: vc)
        present(dest, animated: true, completion: nil)
    }
    
}

extension UITableViewCell {
    
    func setup(_ topic: Topic) -> Self {
        detailTextLabel?.lineBreakMode = .byWordWrapping
        detailTextLabel?.text = topic.summary
        detailTextLabel?.numberOfLines = 10
        textLabel?.text = topic.title
        return self
    }
    
}

extension HomeViewController {
    
    func animateBar() {
        
        UIView.animate(withDuration: 0.5, animations: view.layoutIfNeeded) { _ in
            self.pathView.performBatchUpdates(self.updates, completion: self.completion)
        }
        
    }
    
    func completion(_ idk: Bool) {
        if presenter.pathTopics.count != 0 {
            let indexPath = IndexPath(item: pathView.numberOfItems(inSection: 0) - 1, section: 0)
            pathView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    func updates() {
        pathView.reloadData()
        pathView.numberOfItems(inSection: 0)
        if let topic = presenter.currentTopic {
            var tempTopics : [Topic] = []
            var cur = presenter.currentTopic
            while cur != nil {
                tempTopics.append(cur!)
                cur = topicData.find(id: cur!.parentId)
            }
            presenter.pathTopics = tempTopics.reversed()
        } else {
            presenter.pathTopics = []
        }
        
        pathView.reloadSections(IndexSet(NSIndexSet(index: 0)))
    }
    
}


