//
//  FeedbackPresenter.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol HomeView: BaseView {
    func goToContent(_ content: Content, _ topic: Topic)
    func updateBarItems()
    func reloadTableSection(_ index: Int, slideIn: Bool)
    func playSystemSound()
}

class HomePresenter {
    
    let view: HomeView
    
    var searchTopics: [(Topic, String, String)]
    var currentSearchTag: String
    var currentTopics: [Topic]
    var currentParentId: Int
    var currentTopic: Topic?
    var pathTopics: [Topic]
    var activeSearch: Bool
    
    init(view: HomeView) {
        self.view = view
        self.pathTopics = []
        self.searchTopics = []
        self.activeSearch = false
        self.currentParentId = -1
        self.currentSearchTag = ""
        self.currentTopics = topicData.findWhere(parentId: 0)
    }
    
    func updateTopic() -> String {
        if let topic = currentTopic {
            if let newTopic = topicData.find(id: topic.parentId) { return setNewTopic(newTopic) }
            else { return setParentTopic() }
        }
        return ""
    }
    
    func setNewTopic(_ topic: Topic) -> String {
        currentTopic = topic
        currentTopics = topicData.findWhere(parentId: Int(topic.id))
        return currentTopic?.title ?? ""
    }
    
    func setParentTopic() -> String {
        currentTopic = nil
        currentTopics = topicData.findWhere(parentId: 0)
        return "Topics"
    }
    
    func handleSelectingTopic(_ index: Int, _ fromSearch: Bool) {
        let topic = currentTopics[index]
        if let content = contentData.findWhere(topicId: topic.id){
            currentTopic = topicData.find(id: topic.parentId)
            view.goToContent(content, topic)
            return
        }
        let newTopics = topicData.findWhere(parentId: Int(topic.id))
        
        if !newTopics.isEmpty {
            currentTopic = topic
            currentTopics = newTopics
            view.updateBarItems()
            view.reloadTableSection(0, slideIn: true)
        }
        else {
            if fromSearch {
                currentTopic = topicData.find(id: topic.parentId)
                view.updateBarItems()
            }
            view.playSystemSound()
        }
    }
    
    func topicsCount() -> Int {
        if pathTopics.count == 0 { return 0 }
        else if pathTopics.count == 1 { return 1 }
        else { return pathTopics.count * 2 - 1 }
    }
    
    func postFeedbackIfNeeded() {
        feedbackData.list().forEach { feedback in
            api.postFeedback(feedback: feedback, view.onResponse(
                doOnSuccess: { feedbackData.delete(feedback: feedback) },
                doOnError: view.showError
            ))
        }
    }
    
    func searchCallback(_ clearedSearch: Bool, _ results: [(Topic, String, String)], _ tag: String, _ topic: Topic?) {
        activeSearch = !clearedSearch
        searchTopics = activeSearch ? results : []
        currentSearchTag = activeSearch ? tag : ""
        if activeSearch {
            currentSearchTag = tag
            if let topic = topic {
                currentTopics = topicData.findWhere(parentId: Int(topic.parentId))
                view.reloadTableSection(0, slideIn: true)
                for (index, listedTopic) in currentTopics.enumerated() {
                    if listedTopic == topic { handleSelectingTopic(index, true) }
                }
            }
        }
    }
    
    func onCancel() { view.goBack() }
    
}

