//
//  TitleLabelCollectionViewCell.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/20/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class TitleLabelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
}

