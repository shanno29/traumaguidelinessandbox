//
//  SearchViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/14/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

typealias SearchCallback = ((Bool, [(Topic, String, String)], String, Topic?) -> Void)

class SearchViewController: BaseVC {
  
    @IBOutlet weak var tableView: UITableView!

    var callBack : SearchCallback?
    var newSearch : Bool = false
    var tag: String = ""
    var results: [(Topic, String, String)] = []
    var currentTopic: Topic?
  
    static func setup(_ homePresenter: HomePresenter) -> SearchViewController {
        let vc = SearchViewController.inflate()
        vc.currentTopic = homePresenter.currentTopic
        vc.callBack = homePresenter.searchCallback
        vc.newSearch = homePresenter.activeSearch
        vc.results = homePresenter.searchTopics
        vc.tag = homePresenter.currentSearchTag
        vc.modalTransitionStyle = .flipHorizontal
        return vc
    }
  
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        navigationItem.leftBarButtonItem = clearSearchBtn
        navigationItem.rightBarButtonItem = editSearchBtn
        self.title = self.tag
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if newSearch { editSearchOnClick() }
    }
}

//MARK: Helpers
extension SearchViewController {
  
  
    func getSearchResults(tag: String) {
        let searchAll = Prefs.get(entry: .searchAllTopics) as? Bool ?? false
        self.tag = tag
        self.title = "'\(tag)'"
        
        if self.currentTopic == nil {
            self.results = topicData.findWhere(tag: tag)
            self.tableView.reloadData()
            return
        }
        if searchAll {
            self.results = topicData.findWhere(tag: tag)
            self.tableView.reloadData()
            return
        }
        if !searchAll  {
            if let topic = self.currentTopic {
                self.results = topicData.findWhereWithin(topic: topic, tag: tag)
                self.tableView.reloadData()
            }
        }
    }
    
}

//MARK: editSearchBtn
extension SearchViewController {
  
    var editSearchBtn : UIBarButtonItem {
      get { return UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editSearchOnClick)) }
    }
  
    @objc func editSearchOnClick() {
        let alert = UIAlertController(title: "Search", message: "Enter a key to filter results by", preferredStyle: .alert)
        alert.addTextField { (textfield) in
            textfield.textAlignment = .center
        }
        alert.addAction(UIAlertAction(title: "Search", style: .default, handler: { (_) in
            let textField = alert.textFields![0]
            self.getSearchResults(tag: textField.text!)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
            self.dismiss(clearedSearch: true, topic: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK: clearSearchBtn
extension SearchViewController {

  var clearSearchBtn: UIBarButtonItem { get {
      let title = "Clear"
      let action =  #selector(clearButtonTapped)
      return UIBarButtonItem(title: title, style: .plain, target: self, action: action)
  }}
  
  @objc func clearButtonTapped() { dismiss(clearedSearch: true, topic: nil) }
  
}

//MARK: Completion
extension SearchViewController {

    func dismiss(clearedSearch: Bool, topic: Topic?) {
        self.dismiss(animated: true) {
            if let cb = self.callBack {
                cb(clearedSearch, self.results, self.tag, topic)
            }
        }
    }
}

//MARK: UITableView Delegate and DataSource Methods
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return results.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SearchTableViewCell
        let result = results[indexPath.row]
        cell.setup(title: result.0.title ?? "", tags: result.1, path: result.2)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let topic = results[indexPath.row].0
        self.dismiss(clearedSearch: false, topic: topic )
    }
}
