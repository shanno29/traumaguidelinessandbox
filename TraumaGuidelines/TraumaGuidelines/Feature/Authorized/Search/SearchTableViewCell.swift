//
//  SearchTableViewCell.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/21/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var tagsLabel: UILabel!
    
    @IBOutlet weak var pathLabel: UILabel!
    
    func setup(title: String, tags: String, path: String) {
        self.titleLabel.text = title
        self.tagsLabel.text = "Found in \(tags)"
        self.pathLabel.text = "Path: \(path)"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
