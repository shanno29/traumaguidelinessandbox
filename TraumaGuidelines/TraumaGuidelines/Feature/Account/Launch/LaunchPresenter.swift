//
//  LaunchPresenter.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol LaunchView: BaseView {
    func goToHome()
    func goToLogin()
}

class LaunchPresenter {
    
    let view: LaunchView
    
    init(view: LaunchView) { self.view = view }
    
    func attemptAuth(token: Any) {
        let _token = token as? String ?? ""
        _token.isEmpty ? view.goToLogin() : authenticate(_token)
    }
    
    func authenticate(_ token: String) {
        api.authenticate(apiToken: token, view.onResponse(
            doOnSuccess: updateContent,
            doOnError: { title, message in self.view.showError(title, message, action: self.view.goToLogin) }
        ))
    }
    
    func updateContent() {
        api.batchContent(view.onResponse(
            doOnSuccess: view.goToHome,
            doOnError: view.showError
        ))
    }
    
}
