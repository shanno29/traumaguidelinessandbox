//
//  LaunchViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class LaunchViewController: BaseVC, LaunchView  {
    
    var presenter: LaunchPresenter!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter = LaunchPresenter(view: self)
        presenter.attemptAuth(token: Prefs.get(entry: .userToken))
    }
  
}

extension LaunchViewController {
    
    func goToHome() { router.setRoot(HomeViewController.setup(), nav: true) }
    
    func goToLogin() { router.goTo(LoginViewController.setup()) }

}
