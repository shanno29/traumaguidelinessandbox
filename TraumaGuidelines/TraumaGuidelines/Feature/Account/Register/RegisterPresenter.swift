//
//  RegisterPresenter.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol RegisterView: BaseView {
}

public class RegisterPresenter {
    
    let view: RegisterView
    
    init(view: RegisterView) { self.view = view }
    
    func validateInput(_ name: String?, _ email: String?) {
        let _name: String = name ?? ""
        let _email: String = email ?? ""
        
        if _name.isEmpty { view.showError("Blank Name", "Please enter your Name"); return }
        if _email.isEmpty { view.showError("Blank Email", "Please enter your Email"); return }
        register(_name, _email)
    }
    
    func register(_ name: String, _ email: String) {
        api.register(name: name, email: email, view.onResponseWithDialog(
            doOnSuccess: { title, message in
                self.view.showInfo(title, message, self.onCancel)
            },
            doOnError: { title, message in
                self.view.showError(title, message)
                
            }
        ))
    }
    
    func onCancel() { view.goBack() }
    
}
