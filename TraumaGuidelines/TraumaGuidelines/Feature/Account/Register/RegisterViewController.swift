//
//  RegisterViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class RegisterViewController: BaseVC, RegisterView  {

    @IBOutlet weak var chwImageHeight: NSLayoutConstraint!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    var presenter: RegisterPresenter!

    static func setup() -> RegisterViewController {
        let vc = RegisterViewController.inflate()
        vc.presenter = RegisterPresenter(view: vc)
        vc.modalTransitionStyle = .flipHorizontal
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.delegate = self
        nameField.delegate = self
        keyboard.delegate = self
    }
}

//MARK: Events
extension RegisterViewController {
  
    @IBAction func onRegisterClicked() { presenter.validateInput(nameField.text, emailField.text) }
    
    @IBAction func onCancelClicked() { presenter.onCancel() }

}

//MARK: Keyboard
extension RegisterViewController: KeyboardDelegate {
  
  func onKeyboardShown() { chwImageHeight.constant = chwImageHeight.constant / 2 }
  
  func onKeyboardHidden() { chwImageHeight.constant = chwImageHeight.constant * 2 }
  
}
