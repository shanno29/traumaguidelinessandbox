//
//  LoginViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class LoginViewController: BaseVC, LoginView {

    @IBOutlet weak var chwImageHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var presenter: LoginPresenter!
  
    static func setup() -> LoginViewController {
        let vc = LoginViewController.inflate()
        vc.presenter = LoginPresenter(view: vc)
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTextField.delegate = self
        emailTextField.delegate = self
        keyboard.delegate = self
    }
  
}

//MARK: Events
extension LoginViewController {
  
    @IBAction func onLoginClicked() { presenter.validateInput(emailTextField.text, passwordTextField.text) }

    @IBAction func onForgotPasswordClicked() { presenter.onForgotPassword() }

    @IBAction func onRegisterClicked() { presenter.onRegister() }
      
}

//MARK: Nav
extension LoginViewController {
        
    func goToForgotPassword() { router.goTo(ForgotPasswordViewController.setup()) }
    
    func goToHome() { router.setRoot(HomeViewController.setup(), nav: true) }
    
    func goToRegister() { router.goTo(RegisterViewController.setup()) }
    
}

//MARK: Keyboard
extension LoginViewController: KeyboardDelegate {
  
    func onKeyboardShown() { chwImageHeight.constant = chwImageHeight.constant / 2 }
  
    func onKeyboardHidden() { chwImageHeight.constant = chwImageHeight.constant * 2 }
  
}
