//
//  LoginPresenter.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol LoginView: BaseView {
    func goToHome()
    func goToForgotPassword()
    func goToRegister()
}

class LoginPresenter {
    
    let view: LoginView
    
    init(view: LoginView) { self.view = view }
    
    func validateInput(_ email: String?, _ password: String?) {
        let _email: String = email ?? ""
        let _password: String = password ?? ""
        
        if _email.isEmpty { view.showError("Blank Email", "Please enter your Email"); return }
        if _password.isEmpty { view.showError("Blank Password", "Please enter your Password"); return }
        authenticate(_email, _password)
    }
    
    func authenticate(_ email: String, _ password: String) {
        api.authenticate(email: email, password: password, view.onResponse(
            doOnSuccess: updateContent,
            doOnError: { title, msg in self.view.showError(title, msg, action: self.view.goToHome) }
        ))
    }
    
    func updateContent() {
        api.batchContent(view.onResponse(
            doOnSuccess: view.goToHome,
            doOnError: view.showError
        ))
    }
    
    func onForgotPassword() { view.goToForgotPassword() }
    
    func onRegister() { view.goToRegister() }
    
}
