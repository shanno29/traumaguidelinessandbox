//
//  RegisterPresenter.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/22/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol ForgotPasswordView: BaseView {
    //func goToHome()
    //func goToLogin()
}

class ForgotPasswordPresenter {
    
    let view: ForgotPasswordView
    
    init(view: ForgotPasswordView) { self.view = view }
    
    func validateInput(_ email: String?) {
        let _email: String = email ?? ""
        
        if _email.isEmpty { view.showError("Blank Email", "Please Enter Your Email"); return }
        sendForgotPassword(_email)
    }
    
    func sendForgotPassword(_ email: String) {
        api.forgotPassword(email: email, view.onResponseWithDialog(
            doOnSuccess: { title, msg in self.view.showInfo(title, msg, self.onCancel) },
            doOnError: view.showError
        ))
    }
    
    func onCancel() { view.goBack() }
    
}

