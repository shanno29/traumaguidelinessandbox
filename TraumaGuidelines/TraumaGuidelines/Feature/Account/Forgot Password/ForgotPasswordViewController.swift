//
//  ForgotPasswordViewController.swift
//  TraumaGuidelines
//
//  Created by Chase Letteney on 2/7/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseVC, ForgotPasswordView {

    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var emailTextField: UITextField!
    var presenter: ForgotPasswordPresenter!
  
    static func setup() -> ForgotPasswordViewController {
        let vc = ForgotPasswordViewController.inflate()
        vc.presenter = ForgotPasswordPresenter(view: vc)
        vc.modalTransitionStyle = .flipHorizontal
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        keyboard.delegate = self
    }
  
}

//MARK: Events
extension ForgotPasswordViewController {
    
    @IBAction func sendPasswordResetEmailButtonTapped(_ sender: UIButton) { presenter.validateInput(emailTextField.text) }
  
    @IBAction func cancelButtonTapped(_ sender: UIButton) { presenter.onCancel() }

}

//MARK: Keyboard
extension ForgotPasswordViewController: KeyboardDelegate {
  
  func onKeyboardShown() { containerViewHeight.constant = containerViewHeight.constant / 2 }
  
  func onKeyboardHidden() { containerViewHeight.constant = containerViewHeight.constant * 2 }
  
}
