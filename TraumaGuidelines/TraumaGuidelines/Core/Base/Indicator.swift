//
//  Indicator.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class Indicator {
  
  let root: UIView
  
  lazy var spinner: UIActivityIndicatorView = {
    let it = UIActivityIndicatorView()
    it.activityIndicatorViewStyle = .whiteLarge
    it.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
    return it
  }()
  
  lazy var background: UIView = {
    let it = UIView()
    it.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
    it.frame = CGRect(x: 0, y: 0, width: root.frame.width, height: root.frame.height)
    return it
  }()
  
  init(_ view: UIView) {
    root = view
    spinner.center = background.center
    background.addSubview(spinner)
  }
  
  func showLoading() {
    DispatchQueue.main.async {
      self.root.addSubview(self.background)
      self.spinner.startAnimating()
    }
  }
  
  func hideLoading() {
    DispatchQueue.main.async {
      self.spinner.stopAnimating()
      self.background.removeFromSuperview()
    }
  }
  
}
