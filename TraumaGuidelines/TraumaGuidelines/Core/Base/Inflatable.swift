//
//  Inflatable.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol Inflatable: class {
  static var name: String { get }
}

extension Inflatable where Self: UIViewController {
  
  static var name: String {
    return String(describing: self)
  }
  
  static func inflate() -> Self {
    let storyboard = UIStoryboard(name: name, bundle: nil)
    let temp = storyboard.instantiateViewController(withIdentifier: name)
    guard let vc = temp as? Self else { fatalError("Couldn't start storyboard: \(name)") }
    return vc
  }
  
}

extension UIViewController: Inflatable {}
