//
//  Keyboard.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/7/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

protocol KeyboardDelegate {
    func onKeyboardShown()
    func onKeyboardHidden()
}

class Keyboard {
  
  var delegate: KeyboardDelegate? = nil
  var visible: Bool = false;
  var view: UIView
  
  init(_ view: UIView) {
    self.view = view
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name: .UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: .UIKeyboardWillHide, object: nil)
  }
  
  @objc func keyboardShow() {
    if(visible == false) {
      visible = true
      delegate?.onKeyboardShown()
      UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
  }
  
  @objc func keyboardHide() {
    if(visible == true) {
      visible = false
      delegate?.onKeyboardHidden()
      UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
  }
  
}

extension UIViewController: UITextFieldDelegate {
  public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    return textField.resignFirstResponder()
  }
}

extension UIViewController: UITextViewDelegate {
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
