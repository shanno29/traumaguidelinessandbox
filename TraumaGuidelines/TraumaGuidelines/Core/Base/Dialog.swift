//
//  Dialog.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class Dialog {
  
  typealias Callback = () -> Void
  typealias Info = (title: String, msg: String)
  private let view: UIAlertController
  
  init(_ style: UIAlertControllerStyle, _ info: Info) {
    view = UIAlertController(title: info.title, message: info.msg, preferredStyle: style)
  }
  
  func withAction(_ title: String, style: UIAlertActionStyle = .default, _ callback: @escaping Callback = {}) -> Dialog {
    let action = UIAlertAction(title: title, style: style) { _ in callback() }
    view.addAction(action)
    return self
  }
  
  func show() {
    var currentVC = UIApplication.shared.keyWindow?.rootViewController
    while let presentedVC = currentVC?.presentedViewController { currentVC = presentedVC }
    DispatchQueue.main.async { currentVC?.present(self.view, animated: true, completion: nil) }
  }
  
}

extension Dialog {
  
  static func error(_ info: Info, _ action: Callback? = {}) -> Dialog {
    return Dialog(.alert, info).withActionClose { action?() }
  }
  
  static func info(_ info: Info, _ action: Callback? = {}) -> Dialog {
    return Dialog(.alert, info).withActionOk { action?() }
  }
  
  func withActionOk(_ callback: @escaping Callback = {}) -> Dialog {
    return withAction("Ok", style: .default, callback)
  }
  
  func withActionClose(_ callback: @escaping Callback = {}) -> Dialog {
    return withAction("Close", style: .cancel, callback)
  }
  
}
