//
//  BaseVC.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import Foundation

import UIKit


protocol BaseView {
    
    func onResponse(doOnSuccess: @escaping ()->(), doOnError: @escaping (String, String)->()) -> NetworkResult
    func onResponseWithDialog(doOnSuccess: @escaping (String, String)->(), doOnError: @escaping (String, String)->()) -> NetworkResult
    
    func showInfo(_ title: String, _ msg: String, _ action: Dialog.Callback?)
    func showError(_ title: String, _ msg: String, action: Dialog.Callback?)
    func showError(_ title: String, _ msg: String)
    
    func goBack()

}

class BaseVC: UIViewController, BaseView {
  
    lazy var router = { return Router(self) }()
    lazy var keyboard = { return Keyboard(self.view) }()
    lazy var indicator = { return Indicator(self.view) }()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }
    
    func showError(_ title: String, _ msg: String) { Dialog.error((title, msg), nil).show() }

    func showError(_ title: String, _ msg: String, action: Dialog.Callback? = nil) { Dialog.error((title, msg), action).show() }

    func showInfo(_ title: String, _ msg: String, _ action: Dialog.Callback? = nil) { Dialog.info((title, msg), action).show() }

    func onResponse(doOnSuccess: @escaping ()->(), doOnError: @escaping (String, String)->() = {_,_ in }) -> NetworkResult {
        indicator.showLoading()
        return { success, title, message in
            self.indicator.hideLoading()
            success ? doOnSuccess() : doOnError(title, message)
        }
    }
    
    func onResponseWithDialog(doOnSuccess: @escaping (String, String)->(), doOnError: @escaping (String, String)->() = {_,_ in }) -> NetworkResult {
        indicator.showLoading()
        return { success, title, message in
            self.indicator.hideLoading()
            success ? doOnSuccess(title, message) : doOnError(title, message)
        }
    }
    
    func goBack() {
        router.goBack()
    }
  
}
