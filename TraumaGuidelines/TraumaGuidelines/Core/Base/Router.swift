//
//  Nav.swift
//  TraumaGuidelines
//
//  Created by Matthew Shannon on 2/5/18.
//  Copyright © 2018 Chase Letteney. All rights reserved.
//

import UIKit

class Router {
  
  let parent: UIViewController
  
  init(_ vc: UIViewController) { parent = vc }
  
  func setRoot(_ vc: UIViewController, nav: Bool) {
    let rootVC = nav ? UINavigationController(rootViewController: vc) : vc
    parent.removeFromParentViewController()
    parent.present(rootVC, animated: true)
  }
  
  func goTo(_ vc: UIViewController) {
    if let nav = parent.navigationController { nav.pushViewController(vc, animated: true) }
    else { parent.present(vc, animated: true) }
  }
  
  func goBack() {
    if let nav = parent.navigationController { nav.popViewController(animated: true) }
    else { parent.dismiss(animated: true) }
  }
  
  func showBackButton(_ visible: Bool) {
    guard let nav = parent.navigationController else { return }
    nav.navigationItem.setHidesBackButton(!visible, animated: true)
  }
  
}
